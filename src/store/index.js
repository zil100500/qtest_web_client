import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user';
import console from "@/store/modules/console";
import ws from '@/store/modules/ws';
import reports from "@/store/modules/reports";
import probes from "@/store/modules/probes";

Vue.use(Vuex);

const store = new Vuex.Store({

    state: {

    },

    getters: {

    },

    mutations: {

    },

    actions: {

    },

    modules: {
        ws,
        user,
        console,
        reports,
        probes
    }

});

export default store;
