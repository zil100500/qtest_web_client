const state = {
    isClosed: true,
    content: ''
};

const getters = {

    isClosed: state => {
        return state.isClosed;
    },

    content: state => {
        return state.content;
    }

};

const mutations = {

    close(state) {
        state.isClosed = true;
    },

    open(state) {
        state.isClosed = false;
    },

    /**
     * @param state
     * @param {{incoming: boolean, payload: string}} message
     */

    pushMessage(state, message) {

        state.content = `${state.content}${message.incoming ? '<' : '>'} ${message.payload}\r\n`;
    }

};

const actions = {

    pushConsoleMessage: {
        root: true,
        handler(context, payload) {
            context.commit('pushMessage', payload);
        }
    }

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
