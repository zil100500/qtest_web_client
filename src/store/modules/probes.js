const state = {
    items: [],
};

const getters = {

    items: state => {
        return state.items;
    }

};

const mutations = {

    setItems(state, items) {
        state.items = items;
    }

};

const actions = {

    async fetchItems(context) {

        await context.dispatch('sendWSMessage', {
            action: 'fetch-probes'
        }, {root: true});

    },

    setProbes: {
        root: true,
        handler(context, items) {
            context.commit('setItems', items);
        }
    }

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
