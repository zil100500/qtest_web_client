const state = {
    socket: null,
    connected: false,
    incoming: null
};

const getters = {


};

const mutations = {

    setSocket(state, socket) {
        state.socket = socket;
    },

    connect(state) {
        state.connected = true;
    },

    disconnect(state) {
        state.connected = false;
    },

    setIncoming(state, message) {
        state.incoming = message;
    }

};

const actions = {

    async connect(context) {

        if (context.state.ws) {
            return;
        }

        let socket = new WebSocket(process.env.VUE_APP_NEXUS_URL);
        context.commit('setSocket', socket);

        socket.onopen = () => {
            context.commit('connect');
            context.dispatch('pushConsoleMessage', {
                incoming: false,
                payload: 'WS connected'
            }, {root: true});
        }

        socket.onclose = () => {
            context.commit('setSocket', null);
            context.commit('disconnect');
            context.dispatch('pushConsoleMessage', {
                incoming: false,
                payload: 'WS disconnected'
            }, {root: true});
        }

        socket.onmessage = event => {

            context.commit('console/pushMessage', event.data, {root: true});

            try {
                let data = JSON.parse(event.data);
                context.dispatch('routeIncoming', data);
            }
            catch (error) {
                context.dispatch('pushConsoleMessage', {
                    incoming: true,
                    payload: `Unable to parse message. Error: ${error.message}`
                }, {root: true});
            }
        }

        // socket.onerror = error => {
        //     //TODO handle error
        // }

    },

    async routeIncoming(context, message) {

        if (!message || !message.action) {
            throw  new Error('Invalid message format');
        }

        await context.dispatch('pushConsoleMessage', {
            incoming: true,
            payload: JSON.stringify(message)
        }, {root: true});

        switch (message.action) {
            case 'admin-auth':
                if (message.ok) {
                    await context.dispatch('changeUserAuthStatus', true, {root: true});
                }
                else {
                    await context.dispatch('setUserAuthError', 'Invalid token.', {root: true});
                }
                break;
            case 'reports':
                await context.dispatch('setReports', message.data, {root: true});
                break;
            case 'probes':
                await context.dispatch('setProbes', message.data, {root: true});
                break;
            default:
                console.log(message);
        }


    },

     sendWSMessage: {
        root: true,
        handler: async (context, message) => {

            try {
                context.state.socket.send(JSON.stringify(message));
                await context.dispatch('pushConsoleMessage', {
                    incoming: false,
                    payload: JSON.stringify(message)
                }, {root: true});
            }
            catch(error) {
                //TODO  handler
                console.log(error);
            }

        }
    }

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
