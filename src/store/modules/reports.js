const state = {
    items: [],
    filteredItems: [],
    start: null,
    end: null
};

const getters = {

    items: state => {
        return state.items;
    },

    filteredItems: state => {
        return state.filteredItems;
    },

    start: state => {
        return state.start;
    },

    end: state => {
        return state.end;
    }

};

const mutations = {

    setItems(state, items) {
        state.items = items;
    },

    setFilteredItems(state, items) {
        state.filteredItems = items;
    },

    setStart(state, start) {
        state.start = start;
    },

    setEnd(state, end) {
        state.end = end;
    },

    filter(state) {

        //TODO server side filter + pagination

        let items = state.items.sort((a, b) => {
            return b.timestamp - a.timestamp;
        });

        items = items.filter(item => {

            if (state.start) {
                if (item.timestamp < state.start) {
                    return false;
                }
            }

            if (state.end) {
                if (item.timestamp > state.end) {
                    return false;
                }
            }

            return true;

        });

        state.filteredItems = items;


    }

};

const actions = {

    async fetchItems(context, query) {

        await context.dispatch('sendWSMessage', {
            action: 'fetch-reports',
            query
        }, {root: true});

    },

    setStart(context, start) {

        context.commit('setStart', start);
        context.commit('filter');

    },

    setEnd(context, end) {

        context.commit('setEnd', end);
        context.commit('filter');

    },

    setReports: {
        root: true,
        handler(context, items) {
            context.commit('setItems', items);
            context.commit('setFilteredItems', items);
            context.commit('filter');
        }
    }

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
