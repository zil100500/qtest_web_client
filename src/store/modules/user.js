const state = {
    authenticated: false,
    authError: '',
    roles: []
};

const getters = {

    hasRole: state => role => {
        return state.roles.indexOf(role) !== -1;
    },

    getRoles: state => {
        return state.roles;
    },

    authenticated: state => {
        return state.authenticated;
    },

    authError: state => {
        return state.authError;
    }

};

const mutations = {

    initUserRole(state) {
        state.roles = ['ROLE_USER']
    },

    setAuthStatus(state, status) {
        state.authenticated = status;
    },

    setAuthError(state, error) {
        state.authError = error;
    }

};

const actions = {

    async login(context, token) {

        await context.dispatch('sendWSMessage', {
            action: 'admin-auth',
            token: token
        }, {root: true});

    },

    changeUserAuthStatus: {
        root: true,
        handler(context, isAuthenticated) {
            context.commit('setAuthStatus', isAuthenticated);
            if (isAuthenticated) {
                context.commit('initUserRole');
            }
        }
    },

    setUserAuthError: {
        root: true,
        handler(context, error) {
            context.commit('setAuthError', error);
        }
    }

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
