import VueRouter from 'vue-router';
import Vue from "vue";
import store from './store';
Vue.use(VueRouter);

const guard = (grants, next, redirectRoute = '/login') => {

    for (let grant of grants) {
        if (store.getters['user/getRoles'].indexOf(grant) !== -1) {
            next();
            return;
        }
    }

    next(redirectRoute);

};

const routes = [
    {
        path: '',
        redirect: '/probes'
    },
    {
        path: '/login',
        component:  () => import('@/components/login'),
        beforeEnter: (to, from, next) => {
           if (store.getters['user/authenticated']) {
               next(from.path);
           }
           else {
               next();
           }
        }
    },
    {
        path: '/probes',
        component: () => import('@/components/probes'),
        beforeEnter: (to, from, next) => {
            guard(['ROLE_USER'], next);
        }
    },
    {
        path: '/reports',
        component: () => import('@/components/reports'),
        beforeEnter: (to, from, next) => {
            guard(['ROLE_USER'], next);
        }
    }
];

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {

    if (store.getters['user/authenticated'] === null) {
        store.watch(() => store.getters['user/authenticated'], () => {
            next();
        });
    }
    else {
        next();
    }

});

export default router;
