import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import store from './store';
import router from './router';
import VueMoment from 'vue-moment'
import moment from 'moment';
import 'moment/locale/ru';

Vue.use(VueMoment, {moment});

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
